"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const scenegraph_1 = require("scenegraph");
function rectangleHandlerFunction(selection) {
    const newElement = new scenegraph_1.Rectangle(); // [3]
    newElement.width = 100;
    newElement.height = 50;
    newElement.fill = new scenegraph_1.Color("Purple");
    selection.insertionParent.addChild(newElement); // [4]
    newElement.moveInParentCoordinates(100, 100); // [5]
}
function ellipseHandlerFunction(selection) {
    const newElement = new scenegraph_1.Ellipse();
    newElement.radiusX = 150;
    newElement.radiusY = 100;
    newElement.fill = new scenegraph_1.Color("Purple");
    selection.insertionParent.addChild(newElement);
    newElement.moveInParentCoordinates(100, 300);
}
(module).exports = {
    commands: {
        createRectangle: rectangleHandlerFunction,
        createEllipse: ellipseHandlerFunction
    }
};
